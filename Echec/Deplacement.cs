﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JeuSociete;

namespace Echec
{
    public class Deplacement
    {
        public Piece PieceCaseOrigine { get; set; }
        public Piece ContenuCaseDestination { get; set; }
        public SegmentPlateau Segment { get; set; }
        public Deplacement(SegmentPlateau segment, Piece caseOrigine, Piece caseDestination)
        {
            Segment = segment;
            ContenuCaseDestination = caseDestination;
            PieceCaseOrigine = caseOrigine;
        }

        public TypesPiece ObtenirTypePieceChoisie()
        {
            return PieceCaseOrigine.Type;
        }

        public bool PieceChoisiePeutSauter()
        {
            return PieceCaseOrigine.PeutSauter();
        }

        public Couleur ObtenirCouleurPieceChoisie()
        {
            return PieceCaseOrigine.Couleur;
        }
    }
}
