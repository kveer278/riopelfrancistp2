﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JeuSociete;

namespace Echec
{
    public class Cavalier:Piece
    {
        
        public  override TypesPiece Type
        {
            get
            {
                return TypesPiece.Cavalier;
            }
        }
        
        public override bool DeplacementPossible(SegmentPlateau deplacementVoulu)
        {
            if (MouvementPossible(deplacementVoulu, 1, 2) || MouvementPossible(deplacementVoulu, 2, 1)) //pour faire des L sur l'échiquier
            {
                return true;
            }
            return false;
        }
        
        public override bool PeutSauter()
        {
            return true;
        }

        private bool MouvementPossible(SegmentPlateau deplacementVoulu, int x, int y)
        {
            int origineX = deplacementVoulu.Origine.X;
            int origineY = deplacementVoulu.Origine.Y;
            int destinationX = deplacementVoulu.Destination.X;
            int destinationY = deplacementVoulu.Destination.Y;
            if (destinationX == origineX - x || destinationX == origineX + x)
            {
                if (destinationY == origineY - y || destinationY == origineY + y)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
