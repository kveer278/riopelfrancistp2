﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JeuSociete;

namespace Echec
{
    public class Pion : Piece
    {
        public  override TypesPiece Type { 
            get { 
                return TypesPiece.Pion; 
            } 
        }

        public override bool DeplacementPossible(SegmentPlateau deplacementVoulu)
        {
            int sensY = this.Couleur == Couleur.Blanc ? 1 : -1;
            if(EstMouvementReculons(deplacementVoulu, sensY)){
                return false;
            }
            if (EstMouvementStandard(deplacementVoulu) || 
                EstDeplacement2Cases(deplacementVoulu) ||
                EstCapture(deplacementVoulu))
            {
                return true;
            }
            return false;
        }

        private bool EstMouvementStandard(SegmentPlateau deplacementVoulu)
        {
            if (!deplacementVoulu.EstSegmentDroit() || deplacementVoulu.EstSegmentHorizontal())
            {
                return false;
            }
            int nombreCasesDeplacement = deplacementVoulu.NombreCases();
            bool estMouvementStandard = deplacementVoulu.EstSegmentDroit() && nombreCasesDeplacement == 1;
            return estMouvementStandard;
        }

        private bool EstCapture(SegmentPlateau deplacementVoulu)
        {
            if (!deplacementVoulu.EstSegmentDiagonal45Degres())
            {
                return false;
            }
            int nombreCasesDeplacement = deplacementVoulu.NombreCases();
            bool estCaptureValide = deplacementVoulu.EstSegmentDiagonal45Degres() && nombreCasesDeplacement == 1;
            return estCaptureValide;
        }

        private bool EstDeplacement2Cases(SegmentPlateau deplacementVoulu)
        {
            if (!deplacementVoulu.EstSegmentVertical())
            {
                return false;
            }
            int nombreCasesDeplacement = deplacementVoulu.NombreCases();
            //On ne peut faire un mouvement de 2 cases que si le pion n'a pas encore bouger.
            bool estDeplacement2CasesValide = !ADejaBouger && deplacementVoulu.EstSegmentVertical() && nombreCasesDeplacement == 2;
            return estDeplacement2CasesValide;
        }

        private bool EstMouvementReculons(SegmentPlateau deplacementVoulu, int sensY)
        {
            if (sensY == 1 && deplacementVoulu.Destination.Y < deplacementVoulu.Origine.Y)
            {
                return true;
            }
            if (sensY == -1 && deplacementVoulu.Destination.Y > deplacementVoulu.Origine.Y)
            {
                return true;
            }
            return false;
        }
    }
}
