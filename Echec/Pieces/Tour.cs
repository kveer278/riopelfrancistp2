﻿using JeuSociete;

namespace Echec
{
    public class Tour:Piece
    {


        public override TypesPiece Type
        {
            get
            {
                return TypesPiece.Tour;
            }
        }


        public override bool DeplacementPossible(SegmentPlateau deplacementVoulu)
        {
            return deplacementVoulu.EstSegmentDroit();
        }

       
    }
}
