﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JeuSociete;

namespace Echec.Regles
{
    public class PieceMouvementPossible : IRegle
    {
        private Deplacement _deplacement;
        public PieceMouvementPossible(Deplacement deplacement)
        {
            _deplacement = deplacement;
        }

        public bool EstRespectee()
        {
            Piece pieceJouee = _deplacement.PieceCaseOrigine;
            bool piecePeutFaireDeplacement = pieceJouee.DeplacementPossible(_deplacement.Segment);
            if (pieceJouee.Type == TypesPiece.Pion)
            {
                if (EstMouvementPionCaptureDiagonal(piecePeutFaireDeplacement)) //le pion est la seule piece qui ne peut pas capture via sont mouvement normal
                {
                    return false;
                }    
            }
            return piecePeutFaireDeplacement;
        }

        private bool EstMouvementPionCaptureDiagonal(bool piecePeutFaireDeplacement)
        {
            if (piecePeutFaireDeplacement)
            {
                Piece contenuDestination = _deplacement.ContenuCaseDestination;
                if (_deplacement.Segment.EstSegmentDiagonal45Degres() && contenuDestination != null)
                {
                    return true;
                }
            }
            return false;
        }

        public string ObtenirMessageErreur()
        {
            return "La pièce " + _deplacement.ObtenirTypePieceChoisie() + " ne peut pas se déplacer à cet endroit";
        }
    }
}
