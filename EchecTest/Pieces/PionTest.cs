﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Echec;
using JeuSociete;
using System.Collections.Generic;

namespace EchecTest
{
    [TestClass]
    public class PionTest
    {
        [TestMethod]
        public void ConstructeurPion()
        {
            Piece unPion = new Pion();
            unPion.Couleur = Couleur.Blanc;
            unPion.ADejaBouger = true;
            unPion.ADejaBouger = true;
            Assert.AreEqual(TypesPiece.Pion, unPion.Type);
            Assert.AreEqual(Couleur.Blanc, unPion.Couleur);
            Assert.AreEqual(true, unPion.ADejaBouger);
        }

        [TestMethod]
        public void MouvementDeuxCasesMaisADejaBouger()
        {
            int x = 1;
            int y = 2; 
            int yDestination = 4;
            Piece unePiece = new Pion();
            unePiece.ADejaBouger = true;
            CoordonneesPlateau origine = new CoordonneesPlateau(x, y);
            SegmentPlateau mouvement1 = new SegmentPlateau(origine, new CoordonneesPlateau(x, yDestination));
            Boolean mouvementPossible = unePiece.DeplacementPossible(mouvement1);
            Assert.AreEqual(false, mouvementPossible);
        }

        [TestMethod]
        public void MouvementDiagonalPionBlanc()
        {
            int x = 1;
            int xDestination = 2;
            int y = 2;
            int yDestination = 3;
            Piece unePiece = new Pion();
            unePiece.Couleur = Couleur.Blanc;
            CoordonneesPlateau origine = new CoordonneesPlateau(x, y);
            SegmentPlateau mouvement1 = new SegmentPlateau(origine, new CoordonneesPlateau(xDestination, yDestination));
            Boolean mouvementPossible = unePiece.DeplacementPossible(mouvement1);
            Assert.AreEqual(true, mouvementPossible);
        }

        [TestMethod]
        public void MouvementDiagonalePionBlancReculons()
        {
            int x = 1;
            int xDestination = 2;
            int y = 2;
            int yDestination = 1;
            Piece unePiece = new Pion();
            unePiece.Couleur = Couleur.Blanc;
            CoordonneesPlateau origine = new CoordonneesPlateau(x, y);
            SegmentPlateau mouvement1 = new SegmentPlateau(origine, new CoordonneesPlateau(xDestination, yDestination));
            Boolean mouvementPossible = unePiece.DeplacementPossible(mouvement1);
            Assert.AreEqual(false, mouvementPossible);
        }

        [TestMethod]
        public void MouvementNormal1CaseVersAvantBlanc()
        {
            int x = 2;
            int xDestination = 2;
            int y = 2;
            int yDestination = 3;
            Piece unePiece = new Pion();
            unePiece.Couleur = Couleur.Blanc;
            CoordonneesPlateau origine = new CoordonneesPlateau(x, y);
            SegmentPlateau mouvement1 = new SegmentPlateau(origine, new CoordonneesPlateau(xDestination, yDestination));
            Boolean mouvementPossible = unePiece.DeplacementPossible(mouvement1);
            Assert.AreEqual(true, mouvementPossible);
        }

        [TestMethod]
        public void MouvementNormal1CaseVersAvantNoir()
        {
            int x = 2;
            int xDestination = 2;
            int y = 2;
            int yDestination = 1;
            Piece unePiece = new Pion();
            unePiece.Couleur = Couleur.Noir;
            CoordonneesPlateau origine = new CoordonneesPlateau(x, y);
            SegmentPlateau mouvement1 = new SegmentPlateau(origine, new CoordonneesPlateau(xDestination, yDestination));
            Boolean mouvementPossible = unePiece.DeplacementPossible(mouvement1);
            Assert.AreEqual(true, mouvementPossible);
        }

       
      
    }
}
