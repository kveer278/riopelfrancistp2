﻿using System;

namespace JeuSociete
{
    public class CoordonneesPlateau
    {

        public int X { get; set; }
        public int Y { get; set; }

        public CoordonneesPlateau(int x, int y) {
           X = x;
           Y = y;
        }

        public Boolean EstMemeEndroit(CoordonneesPlateau coordonnee)
        {
            if (coordonnee == null)
            {
                return false;
            }
            if (X == coordonnee.X && Y == coordonnee.Y)
            {
                return true;
            }
            return false;
        }
    }
}
