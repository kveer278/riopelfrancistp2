﻿using JeuSociete.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;


namespace JeuSociete
{
    public class PlateauJeuQuadrille<T>
    {
        private Case<T>[][] _cases;
        private int _hauteur;
        private int _largeur;

        public PlateauJeuQuadrille(int hauteur, int largeur)
        {
            _hauteur = hauteur;
            _largeur = largeur;
            _cases = new Case<T>[_hauteur][];
            for (int i = 0; i < _hauteur; i++)
            {
                _cases[i] = new Case<T>[_largeur];
                for (int j = 0; j < _largeur; j++)
                {
                    _cases[i][j] = new Case<T>();
                }
            }
        }

        public Case<T> GetCase(CoordonneesPlateau emplacement)
        {
            if (SontCoordonneesValides(emplacement))
            {
                return _cases[emplacement.X][emplacement.Y];
            }
            else
            {
                throw new CoordonneesHorsPlateauException();
            }
        }

        public T GetContenuCase(CoordonneesPlateau emplacement)
        {
            if (emplacement == null)
            {
                throw new Exception();
            }
            bool horsPlateau = emplacement.X >= _largeur;
                bool horsPlateau2 = emplacement.Y >= _hauteur;
            if(horsPlateau)
            {
                throw new CoordonneesHorsPlateauException();
            }
            return GetCase(emplacement).ContenuCase;
        }

        public void SetContenuCase(CoordonneesPlateau emplacement, T contenu)
        {
            _cases[emplacement.X][emplacement.Y].ContenuCase = contenu;
        }

        //pourquoi avoir utiliser default https://msdn.microsoft.com/en-us/library/xwth0h0d.aspx
        public void DeplacerContenu(SegmentPlateau deplacement, T contenuAMettreOrigine = default(T)) 
        {
            GetCase(deplacement.Destination).ContenuCase = GetCase(deplacement.Origine).ContenuCase;
            GetCase(deplacement.Origine).ContenuCase = contenuAMettreOrigine;
        }

        public IList<CoordonneesPlateau> ObtenirCasesSurChemin(SegmentPlateau segment)
        {
            List<CoordonneesPlateau> cases = new List<CoordonneesPlateau>();
            if (segment.EstSegmentDiagonal45Degres())
            {
                cases = ObtenirCasesEntre2CasesDiagonales(segment);
            }

            if (segment.EstSegmentDroit())
            {
                cases = ObtenirCasesEntre2CasesLigneDroite(segment);
            }
            return cases;
        }

      
        private List<CoordonneesPlateau> ObtenirCasesEntre2CasesLigneDroite(SegmentPlateau segment)
        {
            List<CoordonneesPlateau> cases = new List<CoordonneesPlateau>();
            int offSetX = System.Math.Abs(segment.Origine.X - segment.Destination.X);
            int offSetY = System.Math.Abs(segment.Origine.Y - segment.Destination.Y);
            if (offSetX == 0)
            {
                cases = ObtenirCasesVerticalesEntrePoints(segment);
            }
            else
            {
                cases = ObtenirCasesHorizontalesEntrePoints(segment);
            }
            return cases;
        }

        private List<CoordonneesPlateau> ObtenirCasesEntre2CasesDiagonales(SegmentPlateau segment)
        {
            List<CoordonneesPlateau> cases = new List<CoordonneesPlateau>();
            CoordonneesPlateau pointOuest = (segment.Origine.X < segment.Destination.X) ? segment.Origine : segment.Destination;
            CoordonneesPlateau pointEst = (segment.Origine.X < segment.Destination.X) ? segment.Destination : segment.Origine;
            if (pointOuest.Y < pointEst.Y)
            {
                int nbCases = Math.Abs(pointOuest.X - pointEst.X)-1;
                for (int i = 1; i <= nbCases; i++)
                {
                    cases.Add(new CoordonneesPlateau(pointOuest.X + i, pointOuest.Y + i));
                }
            }
            else
            {
                int nbCases = Math.Abs(pointOuest.X - pointEst.X)-1;
                for (int i = 1; i <= nbCases; i++)
                {
                    cases.Add(new CoordonneesPlateau(pointOuest.X + i, pointOuest.Y - i));
                }
            }
            return cases;
        }

        private List<CoordonneesPlateau> ObtenirCasesHorizontalesEntrePoints(SegmentPlateau segment)
        {
            List<CoordonneesPlateau> cases = new List<CoordonneesPlateau>();
            CoordonneesPlateau pointOuest = (segment.Origine.X < segment.Destination.X) ? segment.Origine : segment.Destination;
            CoordonneesPlateau pointEst = (segment.Origine.X < segment.Destination.X) ? segment.Destination : segment.Origine;
            int nbCases = pointEst.X - pointOuest.X;
            for (int i = 1; i < nbCases; i++)
            {
                cases.Add(new CoordonneesPlateau(pointOuest.X + i, pointOuest.Y));
            }
            return cases;
        }

        private List<CoordonneesPlateau> ObtenirCasesVerticalesEntrePoints(SegmentPlateau segment)
        {
            List<CoordonneesPlateau> cases = new List<CoordonneesPlateau>();
            CoordonneesPlateau pointSud = (segment.Origine.Y < segment.Destination.Y) ? segment.Origine : segment.Destination;
            CoordonneesPlateau pointNord = (segment.Origine.Y < segment.Destination.Y) ? segment.Destination : segment.Origine;
            int nbCases = pointNord.Y - pointSud.Y;
            for (int i = 1; i < nbCases; i++)
            {
                cases.Add(new CoordonneesPlateau(pointSud.X, pointSud.Y + i));
            }
            return cases;
        }

        private bool SontCoordonneesValides(CoordonneesPlateau emplacement)
        {
            return emplacement.X < _largeur || emplacement.X > 0 || emplacement.Y < _hauteur || emplacement.Y > 0;
        }
    }
}
