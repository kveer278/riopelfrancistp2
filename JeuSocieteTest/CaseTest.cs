﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using JeuSociete;

namespace JeuSocieteTest
{
    [TestClass]
    public class CaseTest
    {
        [TestMethod]
        public void ViderCaseVideTest()
        {
            //arrange
            Case<string> caseAVider = new Case<string>();
            //act
            caseAVider.ViderCase();
            //assert
            Assert.AreEqual(true, caseAVider.EstCaseVide());
        }

        [TestMethod]
        public void ViderCaseTest()
        {
            //arrange
            Case<string> caseAVider = new Case<string>("stringbidon");
            //act
            caseAVider.ViderCase();
            //assert
            Assert.AreEqual(null, caseAVider.ContenuCase);
            Assert.AreEqual(true, caseAVider.EstCaseVide());
        }

        [TestMethod]
        public void EstCaseVideTest()
        {
            Case<string> caseATest = new Case<string>(null);
            Assert.AreEqual(false, caseATest.EstCaseVide());
        }
    }
}
